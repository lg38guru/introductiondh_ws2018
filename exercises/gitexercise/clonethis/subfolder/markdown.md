# Learn Markdown
## It's really not that difficult

But always remember you need two spaces  
for a linebreak. And a double return

for a new paragraph.

> Markdown you can use now. (Yoda)

You can use
* *emphasis*
* **bold**

and/or 

1. [links](https://docs.google.com/presentation/d/1dqF9OzMRJPgXJVdf9WWKKMe5wC0ATekJOdoj3Ihjtp0/edit?usp=sharing)
2. sub-lists
    * like so
    * and so

All in one `file.md`

And even ![images](https://upload.wikimedia.org/wikipedia/commons/0/05/Cat_trotting%2C_changing_to_a_gallop.gif)

If that all fails you can use html within markdown, but no markdown in that html:

<div>**doh!**</div>